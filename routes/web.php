<?php

use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::group(['middleware' => 'auth'], function() {
    Route::get('/yard', [DashboardController::class, 'index'])->name('dashboard.index');
    Route::post('/yard', [DashboardController::class, 'checkIn'])->name('dashboard.checkin');
    Route::post('/yard-parking', [DashboardController::class, 'dataParking'])->name('dashboard.parking');
});
