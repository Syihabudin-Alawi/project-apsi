<?php

namespace App\Http\Controllers;

use App\Models\MsDockingTarget;
use App\Models\MsTransactionYardManagement;
use App\Models\MsTransporter;
use App\Models\MsTruck;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class DashboardController extends Controller
{
    public function index()
    {
        $vehicleNumber = MsTruck::where('status', '1')->get();
        $parkingCount  = MsTransactionYardManagement::select('loading_point')
            ->where('loading_point', '!=', NULL)->count();
        $parking = MsTransactionYardManagement::with('msTruck')->where('loading_point', '!=', NULL)->paginate(10);

        return view('pages.dashboard', compact('vehicleNumber', 'parkingCount', 'parking'));
    }

    // public function dataParking(Request $request)
    // {
    //     if ($request->ajax()) {
    //         $parking = MsTransactionYardManagement::with('msTruck', 'msSite')->where('loading_point', '!=', NULL)->get();

    //         return Datatables::of($parking)
    //             ->addIndexColumn()
    //             ->addColumn('msTruck', function ($parking) {
    //                 return $parking->msTruck->plat_number;
    //             })->addColumn('transporter', function ($parking) {
    //                 return $parking->msTruck->msTransporter->company_name;
    //             })->addColumn('truck_type', function ($parking) {
    //                 return $parking->msTruck->msTruckType->truck_type_desc;
    //             })->make(true);
    //     }
    // }

    public function checkIn(Request $request, MsTransactionYardManagement $trx)
    {
        $this->validate($request, [
            'truck_id' => 'required',
            'loading_point' => 'required'
        ]);

        DB::beginTransaction();
        try {
            $truck      = MsTruck::findOrFail($request->truck_id);
            // $tranporter = MsTransporter::findOrFail($truck->transporter_id);
            $dockingTarget  = MsDockingTarget::where('truck_type_id', $truck->truck_type_id)->first();

            if ($trx->count() == 0) {
                $tuNumber = 1;
            } else {
                $tu = $trx::select('tu_number')->latest()->first();
                $tuNumber = $tu->tu_number + 1;
            }

            $truck->update([
                'status' => '0'
            ]);

            $trx->create([
                'site_id' => $dockingTarget->site_id,
                'truck_id' => $request->truck_id,
                'tu_number' => $tuNumber,
                'transction_date_time' => \Carbon\Carbon::now('Asia/Jakarta'),
                'actual_parking_duration' => 5,
                'actual_docking_duration' => 20,
                'late_indicator' => '0',
                'loading_point' => $request->loading_point,
                'parking_target_duration_mins' => 9,
                'avg_parking_time' => 15,
                'target_duration_mins' => $dockingTarget->target_duration
            ]);
            DB::commit();
            toast('Checkin Berhasil', 'success');
            return redirect()->route('dashboard.index');
        } catch (\Throwable $th) {
            DB::rollBack();
            toast('Checkin Gagal', 'error');
            return redirect(route('dashboard.index'));
        }
    }
}
