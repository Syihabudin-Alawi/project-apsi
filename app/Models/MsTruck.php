<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MsTruck extends Model
{
    use HasFactory;

    protected $fillable = ['transporter_id', 'truck_type_id', 'plat_number', 'status'];

    public function msTransactionYardManagement()
    {
        return $this->hasMany(MsTransactionYardManagement::class);
    }

    public function msTransporter()
    {
        return $this->belongsTo(MsTransporter::class, 'transporter_id');
    }

    public function msTruckType()
    {
        return $this->belongsTo(MsTruckType::class, 'truck_type_id');
    }
}
