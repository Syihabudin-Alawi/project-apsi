<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MsDockingTarget extends Model
{
    use HasFactory;

    protected $fillable = ['site_id', 'truck_type_id', 'target_duration'];
}
