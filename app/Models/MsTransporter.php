<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MsTransporter extends Model
{
    use HasFactory;

    protected $fillable = ['company_name', 'address'];

    public function msTruck()
    {
        return $this->hasMany(MsTruck::class);
    }
}
