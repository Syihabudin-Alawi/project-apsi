<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MsTruckType extends Model
{
    use HasFactory;

    protected $fillable = ['truck_type_desc'];

    public function msTruck()
    {
        return $this->hasMany(MsTruck::class);
    }
}
