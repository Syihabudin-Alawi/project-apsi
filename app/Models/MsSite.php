<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MsSite extends Model
{
    use HasFactory;

    protected $fillable = ['site_description', 'site_code', 'utc', 'target_avg_parking', 'target_avg_docking', 'active'];

    public function msTruck()
    {
        return $this->hasMany(MsTruck::class);
    }
}
