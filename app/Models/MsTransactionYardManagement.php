<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MsTransactionYardManagement extends Model
{
    use HasFactory;

    protected $fillable = ['site_id', 'truck_id', 'docking_id', 'tu_number', 'transaction_date_time', 'actual_parking_duration', 'actual_docking_duration', 'late_indicator', 'loading_point', 'free_doors', 'parking_target_duration_mins', 'ship_from', 'delivery_status', 'document_category', 'target_duration_mins', 'avg_parking_time', 'destination'];

    public function msTruck()
    {
        return $this->belongsTo(MsTruck::class, 'truck_id');
    }

    public function msSite()
    {
        return $this->belongsTo(MsSite::class, 'site_id');
    }
}
