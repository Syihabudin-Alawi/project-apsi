<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsTransactionYardManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_transaction_yard_management', function (Blueprint $table) {
            $table->id();
            $table->foreignId('site_id')->constrained('ms_sites')->delete('cascade');
            $table->foreignId('truck_id')->constrained('ms_trucks')->delete('cascade');
            $table->foreignId('docking_id')->constrained('ms_dockings')->delete('cascade');
            $table->integer('tu_number');
            $table->time('transaction_date_time');
            $table->smallInteger('actual_parking_duration');
            $table->smallInteger('actual_docking_duration');
            $table->enum('late_indicator', ['0', '1']);
            $table->string('loading_point', 10)->nullable();
            $table->string('free_doors', 10)->nullable();
            $table->smallInteger('parking_target_duration_mins');
            $table->string('ship_from', 10)->nullable();
            $table->string('delivery_status', 10)->nullable();
            $table->string('document_category', 20)->nullable();
            $table->smallInteger('target_duration_mins');
            $table->smallInteger('avg_parking_time');
            $table->string('destination', 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_transaction_yard_management');
    }
}
