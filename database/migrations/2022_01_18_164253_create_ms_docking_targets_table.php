<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsDockingTargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_docking_targets', function (Blueprint $table) {
            $table->id();
            $table->foreignId('site_id')->constrained('ms_sites')->delete('cascade');
            $table->foreignId('truck_type_id')->constrained('ms_truck_types')->delete('cascade');
            $table->smallInteger('target_duration');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_docking_targets');
    }
}
