<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsTrucksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_trucks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('transporter_id')->constrained('ms_transporters')->delete('cascade');
            $table->foreignId('truck_type_id')->constrained('ms_truck_types')->delete('cascade');
            $table->string('plat_number', 10);
            $table->enum('status', ['0','1']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_trucks');
    }
}
