<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_sites', function (Blueprint $table) {
            $table->id();
            $table->string('site_description', 20);
            $table->string('site_code', 10);
            $table->smallInteger('utc');
            $table->smallInteger('target_avg_parking');
            $table->smallInteger('target_avg_docking');
            $table->enum('active', ['0', '1']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_sites');
    }
}
