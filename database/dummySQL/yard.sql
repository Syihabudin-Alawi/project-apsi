-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 30, 2022 at 03:15 AM
-- Server version: 10.4.12-MariaDB-log
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yard`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2022_01_18_162655_create_ms_transporters_table', 1),
(7, '2022_01_18_162840_create_ms_truck_types_table', 1),
(8, '2022_01_18_163103_create_ms_trucks_table', 1),
(9, '2022_01_18_163548_create_ms_sites_table', 1),
(10, '2022_01_18_164044_create_ms_dockings_table', 1),
(11, '2022_01_18_164253_create_ms_docking_targets_table', 1),
(12, '2022_01_18_164513_create_ms_transaction_yard_management_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ms_dockings`
--

CREATE TABLE `ms_dockings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `site_id` bigint(20) UNSIGNED NOT NULL,
  `docking_description` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ms_dockings`
--

INSERT INTO `ms_dockings` (`id`, `site_id`, `docking_description`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'JK1', '0', NULL, NULL),
(2, 1, 'JK2', '0', NULL, NULL),
(3, 1, 'JK3', '0', NULL, NULL),
(4, 2, 'BDG1', '0', NULL, NULL),
(5, 2, 'BDG2', '0', NULL, NULL),
(6, 2, 'BDG3', '0', NULL, NULL),
(7, 3, 'DPS1', '0', NULL, NULL),
(8, 3, 'DPS2', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ms_docking_targets`
--

CREATE TABLE `ms_docking_targets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `site_id` bigint(20) UNSIGNED NOT NULL,
  `truck_type_id` bigint(20) UNSIGNED NOT NULL,
  `target_duration` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ms_docking_targets`
--

INSERT INTO `ms_docking_targets` (`id`, `site_id`, `truck_type_id`, `target_duration`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 80, NULL, NULL),
(2, 1, 2, 80, NULL, NULL),
(3, 1, 3, 120, NULL, NULL),
(4, 1, 4, 100, NULL, NULL),
(5, 1, 5, 100, NULL, NULL),
(6, 2, 1, 80, NULL, NULL),
(7, 2, 2, 80, NULL, NULL),
(8, 2, 3, 120, NULL, NULL),
(9, 2, 4, 100, NULL, NULL),
(10, 2, 5, 100, NULL, NULL),
(11, 3, 1, 80, NULL, NULL),
(12, 3, 2, 80, NULL, NULL),
(13, 3, 3, 120, NULL, NULL),
(14, 3, 4, 100, NULL, NULL),
(15, 3, 5, 100, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ms_sites`
--

CREATE TABLE `ms_sites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `site_description` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `utc` smallint(6) NOT NULL,
  `target_avg_parking` smallint(6) NOT NULL,
  `target_avg_docking` smallint(6) NOT NULL,
  `active` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ms_sites`
--

INSERT INTO `ms_sites` (`id`, `site_description`, `site_code`, `utc`, `target_avg_parking`, `target_avg_docking`, `active`, `created_at`, `updated_at`) VALUES
(1, 'JAKARTA', 'JKT', 7, 120, 180, '1', NULL, NULL),
(2, 'BANDUNG', 'BDG', 7, 120, 180, '1', NULL, NULL),
(3, 'BALI', 'DPS', 8, 120, 180, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ms_transaction_yard_management`
--

CREATE TABLE `ms_transaction_yard_management` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `site_id` bigint(20) UNSIGNED NOT NULL,
  `truck_id` bigint(20) UNSIGNED NOT NULL,
  `docking_id` bigint(20) UNSIGNED DEFAULT NULL,
  `tu_number` int(11) NOT NULL,
  `transaction_date_time` time NOT NULL DEFAULT current_timestamp(),
  `actual_parking_duration` smallint(6) NOT NULL,
  `actual_docking_duration` smallint(6) NOT NULL,
  `late_indicator` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `loading_point` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `free_doors` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parking_target_duration_mins` smallint(6) NOT NULL,
  `ship_from` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_category` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target_duration_mins` smallint(6) NOT NULL,
  `avg_parking_time` smallint(6) NOT NULL,
  `destination` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ms_transaction_yard_management`
--

INSERT INTO `ms_transaction_yard_management` (`id`, `site_id`, `truck_id`, `docking_id`, `tu_number`, `transaction_date_time`, `actual_parking_duration`, `actual_docking_duration`, `late_indicator`, `loading_point`, `free_doors`, `parking_target_duration_mins`, `ship_from`, `delivery_status`, `document_category`, `target_duration_mins`, `avg_parking_time`, `destination`, `created_at`, `updated_at`) VALUES
(5, 1, 1, NULL, 1, '08:28:58', 5, 20, '0', 'LP1', NULL, 9, NULL, NULL, NULL, 80, 15, NULL, '2022-01-28 18:28:58', '2022-01-28 18:28:58'),
(6, 1, 3, NULL, 2, '10:06:14', 5, 20, '0', 'LP2', NULL, 9, NULL, NULL, NULL, 120, 15, NULL, '2022-01-28 20:06:14', '2022-01-28 20:06:14');

-- --------------------------------------------------------

--
-- Table structure for table `ms_transporters`
--

CREATE TABLE `ms_transporters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ms_transporters`
--

INSERT INTO `ms_transporters` (`id`, `company_name`, `address`, `created_at`, `updated_at`) VALUES
(1, 'ISTANA BUKIT KENCANA MAKMUR, PT', 'BEKASI', NULL, NULL),
(2, 'L001 DUMMY TRANSPORTER', 'TBA', NULL, NULL),
(3, 'PUTRA SINAR DESA, PT', 'BANDA ACEH', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ms_trucks`
--

CREATE TABLE `ms_trucks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transporter_id` bigint(20) UNSIGNED NOT NULL,
  `truck_type_id` bigint(20) UNSIGNED NOT NULL,
  `plat_number` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ms_trucks`
--

INSERT INTO `ms_trucks` (`id`, `transporter_id`, `truck_type_id`, `plat_number`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'B 234 CB', '0', NULL, '2022-01-28 18:28:58'),
(2, 2, 2, 'B 322 AB', '1', NULL, '2022-01-28 18:27:20'),
(3, 3, 3, 'B 322 AB', '0', NULL, '2022-01-28 20:06:14'),
(4, 1, 4, 'B 331 EA', '1', NULL, NULL),
(5, 2, 5, 'B 243 C', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ms_truck_types`
--

CREATE TABLE `ms_truck_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `truck_type_desc` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ms_truck_types`
--

INSERT INTO `ms_truck_types` (`id`, `truck_type_desc`, `created_at`, `updated_at`) VALUES
(1, 'BUILD UP WING BOX (EXTERNAL)', NULL, NULL),
(2, 'CDD (EXTERNAL)', NULL, NULL),
(3, 'TRONTON', NULL, NULL),
(4, 'SINGLE ENGKEL', NULL, NULL),
(5, 'DOUBLE ENGKLE', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin Yard', 'admin@gmail.com', NULL, '$2y$10$g33lyCkQ2JWjGzO392G1qOsSAkQ2429G9bBXMOSjZS.DrD96eBU7e', NULL, NULL, NULL, '2022-01-20 08:46:36', '2022-01-20 08:46:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_dockings`
--
ALTER TABLE `ms_dockings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ms_dockings_site_id_foreign` (`site_id`);

--
-- Indexes for table `ms_docking_targets`
--
ALTER TABLE `ms_docking_targets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ms_docking_targets_site_id_foreign` (`site_id`),
  ADD KEY `ms_docking_targets_truck_type_id_foreign` (`truck_type_id`);

--
-- Indexes for table `ms_sites`
--
ALTER TABLE `ms_sites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_transaction_yard_management`
--
ALTER TABLE `ms_transaction_yard_management`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ms_transaction_yard_management_site_id_foreign` (`site_id`),
  ADD KEY `ms_transaction_yard_management_truck_id_foreign` (`truck_id`),
  ADD KEY `ms_transaction_yard_management_docking_id_foreign` (`docking_id`);

--
-- Indexes for table `ms_transporters`
--
ALTER TABLE `ms_transporters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_trucks`
--
ALTER TABLE `ms_trucks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ms_trucks_transporter_id_foreign` (`transporter_id`),
  ADD KEY `ms_trucks_truck_type_id_foreign` (`truck_type_id`);

--
-- Indexes for table `ms_truck_types`
--
ALTER TABLE `ms_truck_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `ms_dockings`
--
ALTER TABLE `ms_dockings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ms_docking_targets`
--
ALTER TABLE `ms_docking_targets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `ms_sites`
--
ALTER TABLE `ms_sites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ms_transaction_yard_management`
--
ALTER TABLE `ms_transaction_yard_management`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ms_transporters`
--
ALTER TABLE `ms_transporters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ms_trucks`
--
ALTER TABLE `ms_trucks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ms_truck_types`
--
ALTER TABLE `ms_truck_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ms_dockings`
--
ALTER TABLE `ms_dockings`
  ADD CONSTRAINT `ms_dockings_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `ms_sites` (`id`);

--
-- Constraints for table `ms_docking_targets`
--
ALTER TABLE `ms_docking_targets`
  ADD CONSTRAINT `ms_docking_targets_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `ms_sites` (`id`),
  ADD CONSTRAINT `ms_docking_targets_truck_type_id_foreign` FOREIGN KEY (`truck_type_id`) REFERENCES `ms_truck_types` (`id`);

--
-- Constraints for table `ms_transaction_yard_management`
--
ALTER TABLE `ms_transaction_yard_management`
  ADD CONSTRAINT `ms_transaction_yard_management_docking_id_foreign` FOREIGN KEY (`docking_id`) REFERENCES `ms_dockings` (`id`),
  ADD CONSTRAINT `ms_transaction_yard_management_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `ms_sites` (`id`),
  ADD CONSTRAINT `ms_transaction_yard_management_truck_id_foreign` FOREIGN KEY (`truck_id`) REFERENCES `ms_trucks` (`id`);

--
-- Constraints for table `ms_trucks`
--
ALTER TABLE `ms_trucks`
  ADD CONSTRAINT `ms_trucks_transporter_id_foreign` FOREIGN KEY (`transporter_id`) REFERENCES `ms_transporters` (`id`),
  ADD CONSTRAINT `ms_trucks_truck_type_id_foreign` FOREIGN KEY (`truck_type_id`) REFERENCES `ms_truck_types` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
