<!-- Modal Scrollable -->
<div class="modal fade" id="modalPelangganTambah" tabindex="-1" role="dialog"
    aria-labelledby="modalPelangganTambahTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalPelangganTambahTitle">CHECKIN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('dashboard.checkin') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="truck_id">Vehicle Number</label>
                        <select class="form-control @error('truck_id') is-invalid @enderror" name="truck_id" >
                            <option value="" selected disabled>--SELECT PLAT NUMBER--</option>
                            @foreach ($vehicleNumber as $plat)
                            <option value="{{ $plat->id }}">{{ $plat->plat_number }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="loading_point">Loading Point</label>
                        <input type="text" name="loading_point"
                            class="form-control @error('loading_point') is-invalid @enderror" id="loading_point"
                            aria-describedby="Loading Point" value="{{ old('loading_point') }}" placeholder="Input Loading Point">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Check In</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>