<div class="row column1">
    <!-- table section -->
    <div class="col-md-12">
        <div class="white_shd full margin_bottom_30">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>Parking Area</h2>
                </div>
            </div>
            <div class="row pl-2 pr-2">
                <div class="col-md-6 col-lg-6">
                    <div class="full counter_section margin_bottom_30">
                        <div class="couter_icon">
                            <div>
                                <i class="fa fa-cloud-download green_color"></i>
                            </div>
                        </div>
                        <div class="counter_no">
                            <div>
                                <p class="total_no">{{ $parkingCount }}</p>
                                <p class="head_couter">Truck Parked</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="full counter_section margin_bottom_30">
                        <div class="couter_icon">
                            <div>
                                <i class="fa fa-clock-o blue1_color"></i>
                            </div>
                        </div>
                        <div class="counter_no">
                            <div>
                                <p class="total_no">123.50</p>
                                <p class="head_couter">Average Time</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table_section padding_infor_info">
                <div class="table-responsive-sm">
                    <table class="table parking-table">
                        <thead>
                            <tr>
                                <th>Vehicle No.</th>
                                <th>TU Number</th>
                                <th>Transporter</th>
                                <th>Truck Type</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($parking as $p)
                            <tr>
                                <td>{{ $p->msTruck->plat_number }}</td>
                                <td>{{ $p->tu_number }}</td>
                                <td>{{ $p->msTruck->msTransporter->company_name }}</td>
                                <td>{{ $p->msTruck->msTruckType->truck_type_desc }}</td>
                                @if ($p->delivery_status == NULL)
                                <td>
                                    <a class="btn btn-primary btn-sm text-white" data-toggle="modal"
                                        data-target="#modalPelangganTambah" id="#modalScroll">Inbound</a>
                                    <a class="btn btn-warning btn-sm text-white" href="#">Outbound</a>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {!! $parking->links() !!}
            </div>
        </div>
    </div>
</div>