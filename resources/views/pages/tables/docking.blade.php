<div class="row column1">
    <!-- table section -->
    <div class="col-md-12">
        <div class="white_shd full margin_bottom_30">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>Docking Area</h2>
                </div>
            </div>
            <div class="row pl-2 pr-2">
                <div class="col-md-6 col-lg-3">
                    <div class="full counter_section margin_bottom_30">
                        <div class="couter_icon">
                            <div>
                                <i class="fa fa-user yellow_color"></i>
                            </div>
                        </div>
                        <div class="counter_no">
                            <div>
                                <p class="total_no">5/8</p>
                                <p class="head_couter">Dock Occupancy</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="full counter_section margin_bottom_30">
                        <div class="couter_icon">
                            <div>
                                <i class="fa fa-clock-o orange_color"></i>
                            </div>
                        </div>
                        <div class="counter_no">
                            <div>
                                <p class="total_no">1</p>
                                <p class="head_couter">Docking In Progress</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="full counter_section margin_bottom_30">
                        <div class="couter_icon">
                            <div>
                                <i class="fa fa-cloud-download green_color"></i>
                            </div>
                        </div>
                        <div class="counter_no">
                            <div>
                                <p class="total_no">2</p>
                                <p class="head_couter">Docking Complete</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="full counter_section margin_bottom_30">
                        <div class="couter_icon">
                            <div>
                                <i class="fa fa-clock-o blue1_color"></i>
                            </div>
                        </div>
                        <div class="counter_no">
                            <div>
                                <p class="total_no">120:50</p>
                                <p class="head_couter">Average Docking Time</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 pr-2">
                <div class="col-md-2">
                    <div class="card text-center">JK1</div>
                </div>
                <div class="col-md-2">
                    <div class="card text-center">JK2</div>
                </div>
                <div class="col-md-2">
                    <div class="card text-center" style="background-color: green; color: white;">JK3</div>
                </div>
                <div class="col-md-2">
                    <div class="card text-center" style="background-color: yellow; color: white;">BDG1</div>
                </div>
                <div class="col-md-2">
                    <div class="card text-center">BDG2</div>
                </div>
                <div class="col-md-2">
                    <div class="card text-center">BDG3</div>
                </div>
                <div class="col-md-2 pt-2">
                    <div class="card text-center" style="background-color: green; color: white;">DPS1</div>
                </div>
                <div class="col-md-2 pt-2">
                    <div class="card text-center">DPS2</div>
                </div>
            </div>
            <div class="table_section padding_infor_info">
                <div class="table-responsive-sm">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Loading Point</th>
                                <th>Door</th>
                                <th>TU No</th>
                                <th>Destination / ship from</th>
                                <th>Transporter</th>
                                <th>Truck Type</th>
                                <th>Duration</th>
                                <th>Status</th>
                                <th>Late Indicator</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>SEMAR</td>
                                <td>BDG1</td>
                                <td>3</td>
                                <td>NAGA SWALAYAN KRANJI</td>
                                <td>ISTANA KENCANA MAKMUR, PT / BEKASI</td>
                                <td>BUILD UP WING BOX</td>
                                <td>286.01</td>
                                <td><button class="btn btn-warning btn-sm">In Progress</button></td>
                                <td><div style="width: 12px; height: 12px; background: #dd150e;
                                    border-radius: 100%;
                                    margin-top: 0;"></div></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>