@extends('layouts.app', ['title' => 'Dashboard'])
@section('content')
<div class="container-fluid">
    <div class="row column_title">
        <div class="col-md-12">
            <div class="page_title">
                <h2>Dashboard</h2>
            </div>
        </div>
    </div>
    @include('pages.tables.parking')
    @include('pages.tables.docking')
    @include('pages.modals.movement')
</div>
@endsection