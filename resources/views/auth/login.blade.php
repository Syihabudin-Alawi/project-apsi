@extends('layouts.auth', ['title' => 'Login'])
@section('content')
<div class="center verticle_center full_height">
    <div class="login_section">
        <div class="logo_login">
            <div class="center">
                <h2 style="color: white;"><strong>YARD MANAGEMENT</strong></h2>
            </div>
        </div>
        <div class="login_form">
            <form action="{{ route('login') }}" method="POST">
                @csrf
                <fieldset>
                    <div class="field">
                        <label class="label_field">Email Address</label>
                        <input type="email" name="email" placeholder="E-mail" />
                    </div>
                    <div class="field">
                        <label class="label_field">Password</label>
                        <input type="password" name="password" placeholder="Password" />
                    </div>
                    <div class="field margin_0">
                        <label class="label_field hidden">hidden label</label>
                        <button class="main_bt">LOGIN</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
@endsection